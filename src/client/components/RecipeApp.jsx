import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import RecipeItem from './RecipeItem';
import FilterRecipe from './FilterRecipe';
import filterRecipe from '../helpers/filterRecipe';
import { deleteRecipe, deleteAllRecipe } from '../store/actions/recipes';

const RecipeApp = (props) => {
  useEffect(() => {
    window.scrollTo(null, 0);
  }, []);

  const showFavorites = props.location.props ? props.location.props.showFavorites : false;
  const { recipes, filter } = props;
  const recipesToRender = showFavorites ? recipes.filter(r => r.favorite) : recipes;
    
  return (
    <div className="recipe-list-main fadeIn">
      <div className="recipe-list-header">
        <h1>{showFavorites ? "My Favorite recipes" : "My Recipe Box"}</h1>
        <FilterRecipe />
      </div>
      {(filter.title && recipesToRender.length !== 0) && (
        <h4 className="filter__text">
          Found {recipesToRender.length}{` ${recipesToRender.length > 1 ? 'recipes' : 'recipe'}`}
        </h4>
      )}
      <div className="card-wrapper">
        {recipesToRender.length !== 0 && (
          recipesToRender.map(recipe => (
              <RecipeItem 
                  key={recipe.id}
                  recipe={recipe} 
              />
          ))
        )}
      </div>
      {recipesToRender.length === 0 && (
        <div className="recipe__empty">
          <p>
            {showFavorites ? <h2>You have no favorite recipes!</h2> 
            : "Start adding your recipe now. Your data will be saved to your localStorage."}
          </p>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = ({ recipes, filter }) => ({
  recipes: filterRecipe(recipes, filter),
  filter
});

const mapDispatchToProps = dispatch => ({
  deleteRecipe: id => dispatch(deleteRecipe(id)),
  deleteAllRecipe: () => dispatch(deleteAllRecipe())
});

export default connect(mapStateToProps, mapDispatchToProps)(RecipeApp);
