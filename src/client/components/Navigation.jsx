import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Modal from './Modal';

import { deleteAllRecipe } from '../store/actions/recipes';

class Navigation extends Component {
  state = {
    isOpenNavigation: false,
    isOpenModal: false,
    navIcon: 'bars'
  };

  handleDeleteAll = () => {
    this.props.deleteAllRecipe();
    this.props.history.push('/');
    this.setState({ isOpenModal: false });
  };

  openModalHandler = () => {
    this.setState(() => ({ isOpenModal: true }));
  };
  
  closeModalHandler = () => {
    this.setState(() => ({ 
      isOpenModal: false
    }));
  };


  toggleNavigation = () => {
    this.setState({ isOpenNavigation: !this.state.isOpenNavigation });
    this.nav.classList.toggle('open');

    if (this.state.isOpenNavigation) {
      this.setState({ navIcon: 'bars' });
    } else {
      this.setState({ navIcon: 'times' });
    }
  };

  render() {
    const { isOpenNavigation, navIcon } = this.state;

    return (
      <div 
          className="navigation"
          ref={el => this.nav = el}
      >
        <Modal
            isOpenModal={this.state.isOpenModal}
            closeModal={this.closeModalHandler}
        >
          <h2>Delete all recipes?</h2>
          <button 
              className="button--delete"
              onClick={this.handleDeleteAll}
          >
            Yes please delete
          </button>      
        </Modal>
        <div className="navigation-wrapper animate__animated animate__backInDown">
          <div className="navigation-logo">
            <Link to="/">
              <h1>Siva's Cookbook</h1>
            </Link>
          </div>
          <div className="navigation-controls">
            <Link 
                className="button--link"
                exact="true"
                onClick={this.toggleNavigation}
                to={{ 
                  pathname: "/", 
                  props: {
                    showFavorites: false
                  } 
                }} >
              <button className="button--view">View All Recipes</button>
	          </Link>
            <Link 
                className="button--link"
                onClick={this.toggleNavigation}
                to="/addrecipe"
            >
              <button className="button--add">
                New Recipe
              </button>
            </Link>
            <button 
                className="button--delete"
                onClick={this.openModalHandler}
            >
                Delete All Recipes
            </button>
            <Link 
                className="button--link"
                onClick={this.toggleNavigation}
                exact="true"
                to={{ 
                  pathname: "/", 
                  props: {
                    showFavorites: true
                  } 
                }} >
                <button className="button--favorite">Favorite Recipes</button>
	          </Link>
          </div>
        </div>
        <div className="navigation-toggle">
          <button onClick={this.toggleNavigation}>
            <FontAwesomeIcon 
                color="#fff"
                icon={navIcon}
                size="1x"
            />
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  deleteAllRecipe: () => dispatch(deleteAllRecipe())
});

export default withRouter(connect(undefined, mapDispatchToProps)(Navigation));
