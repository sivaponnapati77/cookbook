import React, { Component } from 'react';
import uuid from 'uuid';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TextareaLine from '../helpers/textarealine';
import foodBg from '../assets/images/food.jpg';

class RecipeForm extends Component {
  state = {
    title: this.props.recipe ? this.props.recipe.title : '',
    ingredients: this.props.recipe ? this.props.recipe.ingredients : '',
    numOfServings: this.props.recipe ? this.props.recipe.numOfServings : '',
    cookingTime: this.props.recipe ? this.props.recipe.cookingTime : '',
    recipes: this.props.recipe ? this.props.recipe.recipes.join("\n") : '',
    createdAt: 0,
    image: this.props.recipe ? this.props.recipe.image : '',
    id: this.props.recipe ? this.props.recipe.id : '',
    error: undefined
  };

  componentDidMount() {
    TextareaLine.append_line_numbers(this.textarea.id);
  }
  
  onSubmitHandler = (e) => {
    e.preventDefault();

    if (this.validateForm()) {
      this.setState({ error: '* All fields are mandatory' });
    } else {
      this.setState({ error: undefined });
      this.props.onSubmit({
        title: this.state.title,
        ingredients: this.state.ingredients,
    	  numOfServings: this.state.numOfServings,
	      cookingTime: this.state.cookingTime,
        recipes: this.state.recipes ? this.state.recipes.split("\n")
            .map(s => s.trim())
            .filter(s => s) : [],
        createdAt: moment().valueOf(),
        image: this.state.image,
        id: this.state.id ? this.state.id : uuid()
      });
    }
  };

  validateForm = () => {
    String.prototype.isEmpty = function() {
      return (this.length === 0 || !this.trim());
    };
    return this.state.title.isEmpty()
            || this.state.ingredients.isEmpty()
            || this.state.numOfServings.isEmpty()
            || this.state.cookingTime.isEmpty()
            || this.state.recipes.isEmpty();
  }

  onTitleChange = (e) => {
    const input = e.target.value;
    this.setState({ title: input });
  };

  onIngredientsChange = (e) => {
    const input = e.target.value;
    this.setState({ ingredients: input });
  };
  
  onNumOfServingsChange = (e) => {
    const input = e.target.value;
    this.setState({ numOfServings: input });
  };
  
  onCookingTimeChange = (e) => {
    const input = e.target.value;
    this.setState({ cookingTime: input });
  };

  onRecipeChange = (e) => {
    const input = e.target.value;
    this.setState({ recipes: input });
  };

  onFileChange = (e) => {
    const fileType = e.target.files[0].type;
    if (fileType === 'image/jpeg' || fileType === 'image/png') {
      const reader = new FileReader(); 
      reader.addEventListener('load', () => { 
        this.setState({ image: reader.result });
      });

      reader.readAsDataURL(e.target.files[0]);
    } else {
      alert('File type must be JPEG or PNG');
    }
  };

  render() {
    const { 
      error, 
      title, 
      ingredients,
      numOfServings,
      cookingTime,
      recipes,
      image
    } = this.state;
    return (
      <div>
        <form 
            className="recipe-form"
            onSubmit={this.onSubmitHandler}
        >
          <div className="form-input">
            <br/>
            {error && <span className="error-message"><b>{this.state.error}</b></span>}
            <div className="form-control">
	            <label className="form-label">Recipe Name *</label>
              <input 
                  onChange={this.onTitleChange}
                  type="text" 
                  value={title}
                  autoFocus
              />
            </div>
            <div className="form-control">
       	      <label className="form-label">Ingredients *</label>
              <input 
                  onChange={this.onIngredientsChange}
		              type="text" 
		  type="text" 
		              type="text" 
		  type="text" 
		              type="text" 
		              value={ingredients}
              />
	          </div>
	          <div className="form-control">
       	      <label className="form-label">Number of Servings *</label>
              <input 
                  onChange={this.onNumOfServingsChange}
		              type="number" 
		              value={numOfServings}
              />
            </div>
            <div className="form-control">
       	      <label className="form-label">Cooking time *</label>
              <input 
                  onChange={this.onCookingTimeChange}
		              type="number" 
		              value={cookingTime}
              />
            </div>
            <div className="form-control">
	            <label className="form-label">Recipe Steps *</label>
              <div className="textarea-wrapper">
                <textarea
                    className="textarea-add"
                    id="textarea-add"
                    onChange={this.onRecipeChange} 
                    ref={el => this.textarea = el}
                    rows={recipes ? recipes.split(/\r\n|\r|\n/).length : 0}
                    value={recipes}
                />
              </div>
            </div>
            <div className="form-control">
              <button className="button--add">
                Save Recipe
              </button>
            </div>
          </div>
          <div className="form-thumbnail">
            <img src={image ? image : foodBg} alt=""/>
            <div className="form-file-chooser">
              <label 
                  className="button--block file-label"
                  htmlFor="file"
              >
                Choose Image
              </label>
              <input
                  className="input-file"
                  id="file"
                  onChange={this.onFileChange}
                  type="file"
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default RecipeForm;
