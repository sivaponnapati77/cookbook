# Cookbook Application

1. Clone the project
	> `$ git clone https://sivaponnapati77@bitbucket.org/sivaponnapati77/cookbook.git`

	>`$ cd cookbook`

2. Install Dependencies
	> `$ npm install` 

3. Run development server 
	> `$ npm run dev-server`

Go to http://localhost:8080/ and see the app running

### Usage
When you first open the application there will be no default recipes put in the store. Data preservation in the app is managed with the help of Redux store that benefits from the persisted state communicated to it by the localStorage of the browser. When you open the app in your browser for the firsttime, no default recipes will be visible. Please feel free to add recipes (you can find sample recipes in assets folder under src/client folder)

You can also edit, delete one recipe, delete all recipes and search for recipes. I have created a documentation with the screenshots for above functionality. Please find it at src/client/assets folder with name documentation. 

> For extra credit, I have implemented a basic sorting and add a recipe as favorite functionalities. Please feel free to try those options.

### Architecture
#### General description
Application is written using React.js framework, Redux for global state management.

#### Technologies used
* create-react-app for building
* react-router-dom library for routing
* localStorage for persistent storage of data

[Demo for this application](./demo.md)

[Some sample recipes](./doc/sample_recipes.md)