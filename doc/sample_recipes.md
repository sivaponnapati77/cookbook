
Name : Samosa

Ingredients : Maida, Potato, Peas, Onion, Spices, Green chili, Cheese

Number of servings : 2

Cooking time : 45

Method :
1. Make the samosa dough by mixing add flour, ajwain and salt.
2. Make the filling by mixing potatoes, peas, coriander powder, garam masala, amchur, red chili powder and salt.
3. Fry on low heat. After around 10-12 minutes, the samosa will become firm and light brown in color.

------------------

Name : Indian salad

Ingredients : carrots, radishes, courgettes, red onion, mint leaves, white wine vinegar, Dijon mustard, mayonnaise, olive oil

Number of servings : 2

Cooking time : 20

Method :
1. Grate the carrots into a large bowl. Thinly slice the radishes and courgettes and finely chop the onion. Mix all the vegetables together in the bowl with the mint leaves.
2. Whisk together the vinegar, mustard and mayonnaise until smooth, then gradually whisk in the oil. Taste and add salt and pepper, then drizzle over the salad and mix well. Leftovers will keep in a covered container in the fridge for up to 24 hrs.

-------------------

Name : Fried fish curry

Ingredients : vegetable oil, onions, tomatoes, garlic cloves, coconut milk, coriander, chicken fillets, plain flour, basmati rice to serve

Number of servings : 4

Cooking time : 40

Method :
1. Heat 2 tsp oil in a pan. Tip in the onions and a pinch of salt. Cook for about 8 mins until soft and golden.
2. Meanwhile, blitz the tomatoes, garlic and ginger in a food processor to a smooth purée. Add the curry paste to the onions and fry for 3 mins more. Stir in the tomato mix and simmer for 10 mins until thickened. Add the coconut milk and chopped coriander. Simmer again to thicken.
3. Dust the fish fillets in some seasoned flour. Heat the remaining oil in a non-stick frying pan. Cook the fillets, in batches, over a high heat for 1 min or so on each side, until they begin to brown. Carefully place fish in the tomato mixture and simmer until just cooked through. Scatter over coriander sprigs and serve with rice.

-------------------

Name : Butter chicken

Ingredients : garlic, ginger, green chillies, coriander, onions, turmeric, garam masala, ground cumin, ground fenugreek, chicken breasts

Number of servings : 4

Cooking time : 55

Method :
1. Place the garlic, ginger, chillies and coriander stalks in the small bowl of a food processor with a good pinch of salt
2. Heat 2 tbsp of the ghee or oil in a large pan, add the onions and cook slowly for 15-20 mins until golden and caramelised. Tip in the garlic-ginger paste, turn up the heat and cook for a further 5 mins. Add the ground spices and cook for 2 mins more until fragrant. Scoop the spicy onion mixture out of the pan into a bowl.
3. Add the remaining ghee or oil to the pan and cook the chicken over a medium-high heat until browned all over. Tip into a bowl, cover and set aside.
4. Return the onion mixture to the pan along with the whole spices, tomatoes and a can full of water (400ml). Bring to the boil, then cover and simmer gently for 40 mins.
5. Return the chicken to the pan and cook, uncovered, for a further 10 mins until the sauce has thickened and the chicken is cooked through. Stir in the yogurt and cream, season well and scatter with coriander leaves. Serve with chapatis and rice.

---------------

Name : Vegetable biryani

Ingredients : basmati rice, onions, sunflower oil, ginger, cinnamon sticks, green cardamom pods, star anise, potatoes, cauliflower, Greek yogurt, frozen peas, saffron, butter, 100g roasted & salted cashew nuts, coriander

Number of servings : 6

Cooking time : 45

Method :
1. Rinse the rice in several changes of water to remove excess starch, then put in a bowl of cold water and leave to soak for 30 mins.
2. Meanwhile, fry the onions in the oil for 8 mins until soft and starting to colour. Add the ginger, then cook for 2 mins more. Stir in the curry paste followed by the whole spices, cook for 1 min more, then tip in the potatoes and cauliflower. Pour in 300ml water, cover and boil for about 5-7 mins until the veg are just tender, but still have a little resistance. Stir in the yogurt and peas with 1 tsp salt.
3. Mix the saffron, rosewater and 3 tbsp boiling water together and stir well. Drain the rice, tip into a pan of boiling salted water, then cook for 5 mins until almost tender. Drain again. Get out a large ovenproof dish with a lid and butter the base. Tip the curry sauce into the base, scatter over the nuts, then spoon over the rice. Drizzle over the rosewater mixture, then cover with foil followed by the lid. Can be chilled overnight until ready to cook.
4. Heat oven to 180C/fan 160C/gas 4. Put the biryani in the oven for 45 mins-1 hr until thoroughly heated through. To check it’s ready, try a spoonful from the centre of the rice. For the garnish, slowly fry the onions in the oil until really golden and crisp (this can be done in advance). When the biryani is hot and ready to serve, gently toss through the onions and coriander.









