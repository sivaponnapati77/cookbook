## Add New Recipe
1.	Click on New Recipe button
2.	Fill all the details such as Recipe name, ingredients etc and click on choose image
3.	Click on Save Recipe button

![Add Recipe](./doc/img/1.png)

## View All Recipes
1.	Click on View all recipes button

![View all recipes](./doc/img/2.png)

## View one recipe
1.	Click on any recipe to see all the information about that particular recipe

![View recipe](./doc/img/3.png)

## Edit Recipe
### Before

![Before edit](./doc/img/4.png)

### After
1.	Click on pen icon on a recipe and
2.	Modify one or all the details about a recipe such as recipe name, ingredients, number of servings, cooking time, recipe steps and change image

![After edit](./doc/img/5.png)

## Search for recipe

![Search for recipe](./doc/img/6.png)

## Delete a recipe
1.	Click on a trash icon on a recipe
2.	Click on Yes please delete button to delete the recipe

![Delete a recipe](./doc/img/7.png)

## Delete all recipes
1.	Click on delete all recipes button

![Delete all recipes](./doc/img/8.png)

2.	All the recipes deleted (please see in below screenshot)

![Delete all recipes](./doc/img/9.png)

## Before sort

![Before sort](./doc/img/10.png)

## After Sort by Name in Ascending order

![After sort](./doc/img/11.png)

## Before sort

![Before sort](./doc/img/12.png)

## After Sort by Date in Ascending order

![After sort](./doc/img/13.png)

## Before adding any favorite recipe

![before favorite recipe](./doc/img/14.png)

## Select favorite recipe
1. Click on heart symbol on the recipe to add that as a favorite recipe

![add favorite recipe](./doc/img/15.png)

## View favorite recipes

![view favorite recipes](./doc/img/16.png)
